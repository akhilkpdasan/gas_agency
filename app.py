from flask import Flask, render_template, request, session, url_for, redirect
import MySQLdb

db = MySQLdb.connect(host='localhost', user='root', passwd='', db='gas_agency')
db.autocommit(True)
cursor = db.cursor()
dictcursor = db.cursor(MySQLdb.cursors.DictCursor)
app = Flask(__name__)
app.config['SECRET_KEY'] = '1234556789'

@app.route('/',methods=['GET', 'POST'])
@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        print(session.get('user'))
        if session.get('user'):
            return redirect(url_for('booking'))
        else:
            return render_template('login.html')
    if request.method == 'POST':
        email = request.form['InputEmail']
        password = request.form['InputPassword']
        sql = ''' select password from users where email = '{}';'''.format(email)
        row_count = cursor.execute(sql)
        if row_count == 0:
            return "invalid emali"
        if cursor.fetchone()[0] == password:
            session['user'] = email
            return redirect(url_for('booking'))
        return "Invalid Login"


@app.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    if request.method == 'POST':
        print "entered post stage"
        email = request.form['InputEmail']
        password1 = request.form['InputPassword1']
        password2 = request.form['InputPassword2']
        name = request.form['InputName']
        address = request.form['InputAddress']
        phone = request.form['InputPhone']
        connection_type = request.form['InputConnection']
        if password1 == password2:
            sql = ''' INSERT INTO users VALUES ('{}','{}','{}','{}','{}','{}'); '''.format(email,name,password1,address,phone,connection_type)
            cursor.execute(sql)
            return redirect(url_for('login'))
        else:
            return "pass1 != pass2"
        return "Something went wrong"

@app.route('/booking', methods=['GET','POST'])
def booking():
    if request.method == 'GET':
        return render_template('booking.html')
    if request.method == 'POST':
        cyl_type = request.form['InputType']
        del_date = request.form['InputDate']
        sql = '''select stock from stock where cyl_type='{}';'''.format(cyl_type)
        cursor.execute(sql)
        stock = cursor.fetchone()[0]
        print stock
        if stock > 0:
            waiting = 0
        else:
            waiting = 1
        print waiting
        sql = '''insert into booking values ('','{}',CURDATE(),{},'{}','{}');'''.format(cyl_type,int(waiting),session['user'],del_date)
        cursor.execute(sql)
        return redirect(url_for('booking'))

@app.route('/cancellation', methods=['GET','POST'])
def cancellation():
    if request.method == 'GET':
        sql='''select booking_no from booking where email='{}';'''.format(session['user'])
        cursor.execute(sql)
        bookings = [x for y in cursor.fetchall() for x in y ]
        return render_template('cancellation.html',bookings=bookings)
    if request.method == 'POST':
        booking_no = request.form['InputBooking']
        sql = '''select waiting,cyl_type from booking where booking_no='{}';'''.format(booking_no)
        cursor.execute(sql)
        result = cursor.fetchone()
        cyl_type = result[1]
        waiting = result[0]
        if waiting == '\x00':
            sql = '''select booking_no from booking where cyl_type='{}' and waiting=1 limit 1'''.format(cyl_type)
            row_count = cursor.execute(sql)
            if row_count:
                booking_no = cursor.fetchone()[0]
                sql = '''update booking set waiting = 0 where booking_no='{}';'''.format(booking_no)
                cursor.execute(sql)
            if not row_count:
                cursor.execute('''update stock set stock = stock+1 where cyl_type='{}';'''.format(cyl_type))
        sql = '''delete from booking where booking_no='{}';'''.format(booking_no)
        cursor.execute(sql)
        return redirect(url_for('booking'))


@app.route('/status',methods=['GET','POST'])
def status():
    if request.method == 'GET':
        sql = '''select waiting,count(booking_no) from booking where email='{}' group by waiting;'''.format(session['user'])
        dictcursor.execute(sql)
        results = dictcursor.fetchall()
        print results
        booked,waiting = 0,0
        for result in results:
            if result['waiting'] == '\x00':
                booked = result['count(booking_no)']
            else:
                waiting = result['count(booking_no)']
        return render_template('status.html',booked=booked,waiting=waiting) 

if __name__ == '__main__':
    app.run(debug=True)
